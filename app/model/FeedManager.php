<?php

namespace App\Model;

use Nette;
use Vinelab\Rss;
use League\Csv\Writer;

class FeedManager
{
    /** @var Nette\Database\Context */
    private $db;
    public function __construct(Nette\Database\Context $db)
    {
        $this->db = $db;
    }
    /**
     * @param string $archive_name
     *
     * @return [type]
     */
    public function archiveArticles($archive_name)
    {
        $this->db->query('INSERT INTO `archives`', ['title' => $archive_name]);
        $this->db->query('UPDATE `articles` SET ? WHERE archive_id IS NULL', array('archive_id' => $this->db->getInsertId()));
    }
    /**
     * Creates CSV file for download.
     *
     * @param [type] $articles
     *
     * @return [type]
     */
    public function createCSV($articles)
    {
        if (!is_object($articles) && !is_array($articles)) {
            throw new \Exception('Wrong data type in createCSV expected object or array');
        }
        if (empty($articles)) {
            throw new \Exception('No articles to export (maybe exported already?)');
        }
        $archive_name = date('Y-m-d_H-i-s');
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(['id', 'title', 'link', 'pubDate', 'created']);
        $csv->insertAll($articles);

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="'.$archive_name.'.csv"');
        header('Content-length: '.strlen($csv->__toString()));

        $this->archiveArticles($archive_name);
        $csv->output($archive_name.'.csv');
        die;
    }
    /**
     * @param int    $id
     * @param string $title
     *
     * @return bool
     */
    public function renameArticle(int $id, string $title)
    {
        try {
            $this->db->query('UPDATE `articles` SET ? WHERE id=?', array('title' => $title), $id);
        } catch (Nette\Database\DriverException $e) {
            return false;
        }

        return true;
    }
    /**
     * @param int $id
     *
     * @return bool
     */
    public function deleteArticle(int $id)
    {
        try {
            $this->db->query('DELETE FROM `articles` WHERE id=?', $id);
        } catch (Nette\Database\DriverException $e) {
            return false;
        }

        return true;
    }
    /**
     * [getArticles description].
     *
     * @param bool $archived
     *
     * @return [type]
     */
    public function getArticles($archived = false)
    {
        return $this->db->table('articles')->where(($archived ? '' : 'archive_id IS NULL'));
    }

    /**
     * gets the articles from url.
     *
     * @param string $url
     *
     * @return Vinelab\RSS object
     */
    public function grabRss(string $url)
    {
        $rss = new Rss\Rss();
        $feed = $rss->feed($url);

        return $feed->articles();
    }
    /**
     * saves the articles to database.
     *
     * @param RssArticlesCollection $articles
     *
     * @return string response
     */
    public function saveArticles(Rss\ArticlesCollection $articles)
    {
        if (empty($articles)) {
            return 'Žiadne články neboli nájdené';
        }
        $sql = [];
        foreach ($articles as $article) {
            $values[] = ['title' => $article->title, 'link' => $article->link, 'pubDate' => $article->pubDate];
        }

        try {
            $result = $this->db->query('INSERT IGNORE INTO `articles`', $values);
        } catch (Nette\Database\DriverException $e) {
            switch ($e->getDriverCode()) {
                case 1062: return 'Duplicate entry';break;
                default: return 'Error saving to database - '.$e->getMessage().'<br>'.$e->getQueryString();break; // @todo iba koli debugu vymazat/upravit v produkcii
            }
        }

        return 'Pridaných '.$result->getRowCount().' článkov';
    }
}
