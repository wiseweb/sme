<?php

namespace App\Presenters;

use Nette\Application\UI;

class FeedPresenter extends BasePresenter
{
    /** @var FormFactory */
    private $formFactory;
    /** @var FormFactory */
    private $feedManager;
    /** @inject @var \Nette\Utils\Paginator */
    public $paginator;

    /**
     * get injections.
     *
     * @param AppFormsFormFactory $factory
     * @param AppModelFeedManager $feedManager
     */
    public function __construct(\App\Forms\FormFactory $factory, \App\Model\FeedManager $feedManager)
    {
        $this->formFactory = $factory;
        $this->feedManager = $feedManager;
    }
    /**
     * @param int $page
     *
     * @return [type]
     */
    public function renderShow($page = 1)
    {
        $articles = $this->feedManager->getArticles();
        $this->paginator->setItemsPerPage(30); // the number of records on page
        $this->paginator->setItemCount($articles->count());
        $this->paginator->setPage($page);
        $this->template->paginator = $this->paginator;

        $articles->order('created,title');
        $articles->limit($this->paginator->getLength(), $this->paginator->getOffset());
        $this->template->items = $articles->fetchAll();
    }
    /**
     * @return [type]
     */
    public function handleExport()
    {
        $articles = $this->feedManager->getArticles();
        $articles = $articles->fetchAssoc('title');
        $this->feedManager->createCSV($articles);
    }
    /**
     * Edits article title.
     *
     * @param int    $article_id
     * @param string $article_title
     *
     * @return [type]
     */
    public function handleEdit($article_id, $article_title)
    {
        $response = $this->feedManager->renameArticle($article_id, $article_title);
        if (!$response) {
            throw new UI\BadSignalException('Chyba pri premenovaní.');
        }

        $this->redrawControl();
    }
    /**
     * Removes article from database.
     *
     * @param int $article_id
     *
     * @return [type]
     */
    public function handleRemove($article_id)
    {
        $response = $this->feedManager->deleteArticle($article_id);
        if (!$response) {
            throw new UI\BadSignalException('Chyba pri mazaní.');
        }

        $this->redrawControl();
    }
    /**
     * @return [type]
     */
    public function renderUpload()
    {
    }

    /**
     * @return Form
     */
    public function createComponentFeedForm()
    {
        $form = $this->formFactory->create();
        $form->addText('feedUrl', 'Zadajte URL RSS feedu:')
            ->setDefaultValue('http://rss.sme.sk/rss/rss.asp?sek=tech')
            ->addRule(UI\Form::URL, 'URL musí byť v správnom formáte')
            ->setRequired('Prosím zadajte URL.');

        $form->addSubmit('send', 'Odoslať');

        $form->onSuccess[] = [$this, 'feedFormSucceeded'];

        return $form;
    }
    // called after form is successfully submitted
     public function feedFormSucceeded(UI\Form $form, $values)
     {
         $articles = $this->feedManager->grabRss($values->feedUrl);
         $msg = $this->feedManager->saveArticles($articles);
         $this->flashMessage($msg);
         $this->redirect('this');
     }
}
