    function edit(id){
        event.preventDefault();
        var title = prompt("Prosím zadajte nové meno", "Harry Potter");
        if (title == null || title.length==0) {
            return;
        }
        $.nette.ajax({
            url: '?do=edit',
            method: "post",
            data: { article_id: id,article_title:title},
        });
    };
    function remove(id){
        event.preventDefault();
        var confirmation = confirm("Naozaj vymazať?");
        if (confirmation == false) {
            return;
        }
        $.nette.ajax({
            url: '?do=remove',
            method: "post",
            data: { article_id: id},
        });
    };
$(function () {
    $('.popup').click(function(event) {
        event.preventDefault();
        window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");

    });
});
