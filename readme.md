SME Projekt
=============
Vypracovanie zadania na prijimaci pohovor pre SME online. 


Inštalácia
------------
importujte si súbor `sme.sql` do Vašej databázy.

Nainštalujte si composer podľa [inštrukcií](https://doc.nette.org/composer). Následne v príkazovom riadku zadajte:

	cd path/to/package
	composer install


Ubezpečte sa, že máte priečinky `temp/` a `log/` zapisovateľné.
Vytvorte si súbor `app/config/config.local.neon` s nastaveniami pripojenia k databáze napr.:

	parameters:

	database:
		dsn: 'mysql:host=localhost;dbname=sme'
		user: root
		password: ""
		options:
			lazy: yes


